CMD=$0
if [ "${0::1}" == "-" ]; then
    CMD=${0:1}
fi

CUR_DIR=$(realpath $(dirname $CMD))

if [ "$(ls $CUR_DIR/armory-boot-usb 2>/dev/null | wc -l)" == "0" ]; then
    export ROOT_DIR=$CUR_DIR
else
    export ROOT_DIR=$(realpath $CUR_DIR/../)
fi

export BUILD_ENV=$ROOT_DIR/build
export IMAGES=$ROOT_DIR/images
export TAMAGO=$ROOT_DIR/toolchain/tamago-go/bin/go
export GO_COMPILER=$TAMAGO
export CROSS_COMPILE=$ROOT_DIR/toolchain/gcc-armv7/bin/arm-none-eabi-
export PATH=$BUILD_ENV:$(dirname $TAMAGO):$PATH
export BOOT=eMMC
export START=5242880

croot() {
    cd $ROOT_DIR
}

armory_tool() {
    sudo $BUILD_ENV/armory-boot-usb -i $1
}

armory_fetch_count() {
    sudo dd if=$1 of=$2.count bs=1 skip=1016 count=8
}

armory_update_count() {
    sudo dd if=$1 of=$2 bs=1 seek=1016 count=8 conv=notrunc
}

armory_ums() {
    armory_tool $IMAGES/armory-ums-signed.imx
}

armory_emmc_image() {
    IMG=$1
    DEV=$2
    armory_fetch_count $DEV emmc

    if [ "${IMG:(-2)}" == "xz" ]; then
        xzcat $IMG | sudo dd of=$DEV status=progress
    else
        sudo dd if=$IMG of=$DEV status=progress
    fi

    armory_update_count emmc.count $DEV

    if [ -f emmc.count ]; then
        sudo rm emmc.count
    fi
}