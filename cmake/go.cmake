cmake_minimum_required(VERSION 3.15)

function(add_go_prebuild)
    set(options OPTION)
    set(singleArgs TARGET CMD)
    set(multiArgs ACTION)

    cmake_parse_arguments( PB "${options}" "${singleArgs}" "ACTION" ${ARGN} )

    add_custom_target(
        ${PB_CMD}
        COMMAND ${PB_ACTION}
    )

    add_dependencies(${PB_TARGET} ${PB_CMD})
endfunction()

function(add_go_exe NAME SOURCES LDFLAG)
    set(OUTNAME ${NAME})
    if("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
        set(OUTNAME ${NAME}.exe)
    endif()

    add_custom_command(
        OUTPUT .${NAME}.gotmp
        COMMAND ${CMAKE_Go_COMPILER} build
        -ldflags="${${LDFLAG}}" -o "${CMAKE_CURRENT_BINARY_DIR}/${OUTNAME}"
        ${${SOURCES}}
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    )

    add_custom_target(${NAME} ALL DEPENDS .${NAME}.gotmp)
    set_property(
        TARGET ${NAME}
        PROPERTY ADDITIONAL_CLEAN_FILES ${OUTNAME}
    )
endfunction()

function(add_go_dll NAME SOURCES LDFLAG)
    set(OUTNAME ${NAME}.so)
    if("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
        set(OUTNAME ${NAME}.dll)
    endif()

    add_custom_command(
        OUTPUT .${NAME}.gotmp
        COMMAND ${CMAKE_Go_COMPILER} build
        -buildmode=c-shared
        -ldflags="${${LDFLAG}}" -o "${CMAKE_CURRENT_BINARY_DIR}/${OUTNAME}"
        ${${SOURCES}}
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    )

    add_custom_target(${NAME} ALL DEPENDS .${NAME}.gotmp)
    set_property(
        TARGET ${NAME}
        PROPERTY ADDITIONAL_CLEAN_FILES ${OUTNAME}
    )
endfunction()

function(add_go_arm_exe NAME SOURCES LDFLAG)
    set(OUTNAME ${NAME})
    set(GOENV GOOS=linux GOARM=7 GOARCH=arm)

    add_custom_command(
        OUTPUT .${NAME}.gotmp
        COMMAND ${GOENV} ${CMAKE_Go_COMPILER} build
        -ldflags="${${LDFLAG}}" -o "${CMAKE_CURRENT_BINARY_DIR}/${OUTNAME}"
        ${${SOURCES}}
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    )

    add_custom_target(${NAME} ALL DEPENDS .${NAME}.gotmp)
    set_property(
        TARGET ${NAME}
        PROPERTY ADDITIONAL_CLEAN_FILES ${OUTNAME}
    )
endfunction()